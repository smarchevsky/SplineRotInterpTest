// Copyright Epic Games, Inc. All Rights Reserved.

#include "SplineRotInterpTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SplineRotInterpTest, "SplineRotInterpTest" );
