// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SplineRotInterpTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SPLINEROTINTERPTEST_API ASplineRotInterpTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
