// Fill out your copyright notice in the Description page of Project Settings.

#include "TestCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/LineBatchComponent.h"
#include "Components/TextRenderComponent.h"
#include "DrawDebugHelpers.h"
#include "HeadMountedDisplay.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h"

static void log_screen(const FString& text, const FColor& color, float textTime, float scale = 2.f)
{
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(-1, textTime, color, text, true, FVector2D(scale));
}

static void log_screen(const FString& text, float textTime, float scale = 2.f)
{
    log_screen(text, FColor::White, textTime, scale);
}

// functions SmoothStep, SplineEvalPos, SplineEvalDir implemented in SplineMeshComponent.cpp and LocalVertexFactory.ush
static float SmoothStep(float A, float B, float X)
{
    if (X < A) {
        return 0.0f;
    } else if (X >= B) {
        return 1.0f;
    }
    const float InterpFraction = (X - A) / (B - A);
    return InterpFraction * InterpFraction * (3.0f - 2.0f * InterpFraction);
}

static FVector SplineEvalPos(const FVector& StartPos, const FVector& StartTangent, const FVector& EndPos, const FVector& EndTangent, float A)
{
    const float A2 = A * A;
    const float A3 = A2 * A;

    return (((2 * A3) - (3 * A2) + 1) * StartPos) + ((A3 - (2 * A2) + A) * StartTangent) + ((A3 - A2) * EndTangent) + (((-2 * A3) + (3 * A2)) * EndPos);
}

static FVector SplineEvalDir(const FVector& StartPos, const FVector& StartTangent, const FVector& EndPos, const FVector& EndTangent, float A)
{
    const FVector C = (6 * StartPos) + (3 * StartTangent) + (3 * EndTangent) - (6 * EndPos);
    const FVector D = (-6 * StartPos) - (4 * StartTangent) - (2 * EndTangent) + (6 * EndPos);
    const FVector E = StartTangent;

    const float A2 = A * A;

    return ((C * A2) + (D * A) + E).GetSafeNormal();
}

ATestCharacter::ATestCharacter()
{
    PrimaryActorTick.bCanEverTick = true;

    VROriginComponent = CreateDefaultSubobject<USceneComponent>(TEXT("VROrigin"));
    VROriginComponent->SetupAttachment(GetRootComponent());

    FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
    FirstPersonCameraComponent->SetupAttachment(VROriginComponent);

    m_motionControllerLeft = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController_Left"));
    m_motionControllerLeft->SetupAttachment(VROriginComponent);
    m_motionControllerRight = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController_Right"));
    m_motionControllerRight->MotionSource = FXRMotionControllerBase::RightHandSourceId;
    m_motionControllerRight->SetupAttachment(VROriginComponent);

    m_textRenderLeft = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender_Left"));
    m_textRenderLeft->SetupAttachment(m_motionControllerLeft);
    m_textRenderLeft->SetText(FText());
    m_textRenderRight = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender_Right"));
    m_textRenderRight->SetupAttachment(m_motionControllerRight);
    m_textRenderRight->SetText(FText());

    rollMaxSpeed = 10.f;
    rollL = rollR = 0.f;
    tangentLengthL = tangentLengthR = 50.f;
    changeTangentLengthSpeed = 3.f;
    m_interpolationType = EInterpolationType::CalcDefault;
}

void ATestCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis("IncreaseTangentL", this, &ATestCharacter::IncreaseSizeTangentL);
    PlayerInputComponent->BindAxis("IncreaseTangentR", this, &ATestCharacter::IncreaseSizeTangentR);

    PlayerInputComponent->BindAxis("IncreaseRollL", this, &ATestCharacter::IncreaseRollL);
    PlayerInputComponent->BindAxis("IncreaseRollR", this, &ATestCharacter::IncreaseRollR);

    PlayerInputComponent->BindAction("SwitchInterpolationType", IE_Pressed, this, &ATestCharacter::SwitchInterpolationType);
    /*PlayerInputComponent->BindAction("IncreaseRollL", IE_Pressed, this, &ATestCharacter::RollL<1>);
    PlayerInputComponent->BindAction("DecreaseRollR", IE_Released, this, &ATestCharacter::RollR<0>);*/
}

void ATestCharacter::BeginPlay()
{
    Super::BeginPlay();
    InitialRelOriginLocation = VROriginComponent->GetRelativeLocation();
}

static float ApplyHalfDeadzone(float val)
{
    return val > .5 ? val * 2 - 1
        : val < -.5 ? val * 2 + 1
                    : 0;
}

void ATestCharacter::IncreaseSizeTangentL(float val)
{
    if (val) {
        val = ApplyHalfDeadzone(val);
        auto dt = GetWorld()->GetDeltaSeconds();
        tangentLengthL *= (1 + val * val * val * changeTangentLengthSpeed * dt);
        tangentLengthL = FMath::Clamp(tangentLengthL, 1.f, 1000.f); // from 1cm to 10m
    }
}

void ATestCharacter::IncreaseSizeTangentR(float val)
{
    if (val) {
        val = ApplyHalfDeadzone(val);
        auto dt = GetWorld()->GetDeltaSeconds();
        tangentLengthR *= (1 + val * val * val * changeTangentLengthSpeed * dt);
        tangentLengthR = FMath::Clamp(tangentLengthR, 1.f, 1000.f);
    }
}

void ATestCharacter::IncreaseRollL(float val)
{
    if (val) {
        val = ApplyHalfDeadzone(val);
        auto dt = GetWorld()->GetDeltaSeconds();
        rollL += val * val * val * rollMaxSpeed * dt;
        rollL = FMath::Clamp(rollL, -PI * 10, PI * 10);
        log_screen(FString::SanitizeFloat(dt) + " " + FString::SanitizeFloat(val), 1);
    }
}

void ATestCharacter::IncreaseRollR(float val)
{
    if (val) {
        val = ApplyHalfDeadzone(val);
        auto dt = GetWorld()->GetDeltaSeconds();
        rollR += val * val * val * rollMaxSpeed * dt;
        rollR = FMath::Clamp(rollL, -PI * 10, PI * 10);
        log_screen(FString::SanitizeFloat(dt) + " " + FString::SanitizeFloat(val), 1);
    }
}

void ATestCharacter::SwitchInterpolationType()
{
    uint8 next = (uint8)m_interpolationType + 1;
    m_interpolationType = (EInterpolationType)(next % (uint8)EInterpolationType::MAX);
    FString text = FString::FromInt((int)m_interpolationType);

#define CASE_TO_TEXT(value)         \
    case EInterpolationType::value: \
        text = #value;              \
        break;

    switch (m_interpolationType) {
        CASE_TO_TEXT(CalcDefault)
        CASE_TO_TEXT(CalcWithShortestSpin)
        CASE_TO_TEXT(CalcWithQuatSlerp)
        CASE_TO_TEXT(CalcWithQuatCubic)
    }
    if (!text.IsEmpty())
        log_screen(text, FColor::Red, 2.f, 4.f);
}

// interpolation functions

static FDebugDrawData CalcDefault(const FSplineParams& params, float alpha)
{
    const auto &p0 = params.pos0, &p1 = params.pos1, &d0 = params.dir0, &d1 = params.dir1;
    const auto &roll0 = params.roll0, &roll1 = params.roll1;

    auto pos = SplineEvalPos(p0, d0, p1, d1, alpha);
    auto SplineDir = SplineEvalDir(p0, d0, p1, d1, alpha);

    const FVector BaseX = (params.splineUpDir ^ SplineDir).GetSafeNormal();
    const FVector BaseY = (SplineDir ^ BaseX).GetSafeNormal();

    const float roll = FMath::Lerp(roll0, roll1, alpha);
    const float cosAng = FMath::Cos(roll), sinAng = FMath::Sin(roll);

    const FVector XVec = (cosAng * BaseX) - (sinAng * BaseY);
    const FVector YVec = (cosAng * BaseY) + (sinAng * BaseX);
    return { pos, XVec, YVec };
}

static FDebugDrawData CalcWithShortestSpin(const FSplineParams& params, float alpha)
{
    const auto &p0 = params.pos0, &p1 = params.pos1, &d0 = params.dir0, &d1 = params.dir1;
    const auto &roll0 = params.roll0, &roll1 = params.roll1;

    auto pos = SplineEvalPos(p0, d0, p1, d1, alpha);
    auto splineDir = SplineEvalDir(p0, d0, p1, d1, alpha);

    FVector SpStartX = (params.splineUpDir ^ d0).GetSafeNormal();
    FVector SpEndX = (params.splineUpDir ^ d1).GetSafeNormal();
    FVector SpStartY = (d0 ^ SpStartX).GetSafeNormal();
    FVector SpEndY = (d1 ^ SpEndX).GetSafeNormal();

    float SinStart = FMath::Sin(roll0);
    float CosStart = FMath::Cos(roll0);
    float SinEnd = FMath::Sin(roll1);
    float CosEnd = FMath::Cos(roll1);

    FVector SpStartRotatedX = (CosStart * SpStartX) - (SinStart * SpStartY);
    FVector SpStartRotatedY = (CosStart * SpStartY) + (SinStart * SpStartX);
    FVector SpEndRotatedX = (CosEnd * SpEndX) - (SinEnd * SpEndY);
    FVector SpEndRotatedY = (CosEnd * SpEndY) + (SinEnd * SpEndX);

    float B = (SpStartRotatedX + SpEndRotatedX).Size() * 0.5;
    float C = (SpStartRotatedX - SpEndRotatedX).Size() * 0.5;
    float RenormalizedAlpha = tan(acos(B) * (alpha * 2.0 - 1.0)) * B / C;
    RenormalizedAlpha = RenormalizedAlpha * 0.5 + 0.5;
    if (B == 0 /*opposite direction*/ || C == 0 /*same direction*/)
        RenormalizedAlpha = 0;

    auto XVec = FMath::Lerp(SpStartRotatedX, SpEndRotatedX, RenormalizedAlpha).GetSafeNormal();
    auto YVec = (splineDir ^ XVec).GetSafeNormal();

    return { pos, XVec, YVec };
}

static FDebugDrawData CalcWithQuatSlerp(const FSplineParams& params, float alpha)
{
    const auto &p0 = params.pos0, &p1 = params.pos1, &d0 = params.dir0, &d1 = params.dir1;
    auto pos = SplineEvalPos(p0, d0, p1, d1, alpha);
    auto q = FQuat::Slerp(params.q0, params.q1, alpha);

    return { pos, q.GetAxisX(), q.GetAxisY() };
}

static FDebugDrawData CalcWithQuatCubic(const FSplineParams& params, float alpha)
{
    const auto &p0 = params.pos0, p1 = params.pos1, d0 = params.dir0, d1 = params.dir1;
    auto pos = SplineEvalPos(p0, d0, p1, d1, alpha);
    auto cp0 = params.pos0 + params.dir0;
    auto cp1 = params.pos1 - params.dir1;
    auto diffCP = cp1 - cp0;
    auto q0toDiff = FQuat::FindBetweenVectors(params.dir0, diffCP) * params.q0 * FVector::DotProduct(d0, diffCP);
    auto q1toDiff = FQuat::FindBetweenVectors(params.dir1, diffCP) * params.q1 * FVector::DotProduct(d1, diffCP);
    auto q0 = params.q0 * FVector::DotProduct(d0, d0);
    auto q1 = params.q1 * FVector::DotProduct(d1, d1);

    auto q = FQuat::FastBilerp(q0, q0toDiff, q1toDiff, q1, alpha, alpha).GetNormalized();

    return { pos, q.GetAxisX(), q.GetAxisY() };
}

static void DrawTransform(TArray<FBatchedLine>& lines, FVector p, FQuat q, float lineLength = 15.f, float linethickness = .5f)
{
    lines.Add({ p, p + q.GetAxisX() * lineLength, FColor::Red, -1, linethickness, 0 });
    lines.Add({ p, p + q.GetAxisY() * lineLength, FColor::Green, -1, linethickness, 0 });
    lines.Add({ p, p + q.GetAxisZ() * lineLength, FColor::Blue, -1, linethickness, 0 });
}

void ATestCharacter::DrawSpline(TFunction<FDebugDrawData(const FSplineParams&, float)> func, const FSplineParams& params)
{
    TArray<FBatchedLine> lines;
    auto cp0 = params.pos0 + params.dir0;
    auto cp1 = params.pos1 - params.dir1;
    auto diffCP = cp1 - cp0;

#define DRAW_QUATERNIONS
#ifdef DRAW_QUATERNIONS
    DrawTransform(lines, params.pos0, params.q0);
    DrawTransform(lines, params.pos1, params.q1);

    FQuat q0toDiff = FQuat::FindBetweenVectors(params.dir0, diffCP) * params.q0;
    DrawTransform(lines, cp0, q0toDiff);

    FQuat q1toDiff = FQuat::FindBetweenVectors(params.dir1, diffCP) * params.q1;
    DrawTransform(lines, cp1, q1toDiff);
#endif // DRAW_QUATERNIONS

    // Draw spline base parameters
    lines.Add({ params.pos0, cp0, FColor::White, -1, .2, 0 });
    lines.Add({ params.pos1, cp1, FColor::White, -1, .2, 0 });
    lines.Add({ cp0, cp1, FColor::White, -1, .2, 0 });

    TOptional<FDebugDrawData> prevDebugData;
    for (uint32 i = 0; i <= m_splineResolution; ++i) {
        float alpha = (float)i / m_splineResolution;
        FDebugDrawData debugData = func(params, alpha);

        //DrawDebugLine(GetWorld(), p, p + x * debugLineLenght, FColor::Red, false, -1, 0, .5);
        //DrawDebugLine(GetWorld(), p, p + y * debugLineLenght, FColor::Green, false, -1, 0, .5);

        if (prevDebugData) {
            lines.Add({ debugData.pos, prevDebugData->pos, FColor::Blue, -1, .5, 0 });
            lines.Add({ debugData.pos + debugData.x * debugLineLenght,
                prevDebugData->pos + prevDebugData->x * debugLineLenght, FColor::Red, -1, .5, 0 });
            lines.Add({ debugData.pos + debugData.y * debugLineLenght,
                prevDebugData->pos + prevDebugData->y * debugLineLenght, FColor::Green, -1, .5, 0 });
        }

        prevDebugData = debugData;
    }

    for (uint32 i = 0; i <= m_divsCount; ++i) {
        float alpha = (float)i / m_divsCount;
        FDebugDrawData debugData = func(params, alpha);
        lines.Add({ debugData.pos, debugData.pos + debugData.x * debugLineLenght, FColor::Red, -1, .2, 0 });
        lines.Add({ debugData.pos, debugData.pos + debugData.y * debugLineLenght, FColor::Green, -1, .2, 0 });
    }

    GetWorld()->ForegroundLineBatcher->DrawLines(lines);
}

void ATestCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    // move camera back to origin
    auto offsetCameraPos = InitialRelOriginLocation - FirstPersonCameraComponent->GetRelativeLocation();
    VROriginComponent->SetRelativeLocation(FMath::VInterpTo(VROriginComponent->GetRelativeLocation(),
        offsetCameraPos, DeltaTime, cameraComebackSoftness));

    const auto& transformL = m_motionControllerLeft->GetComponentTransform();
    const auto& transformR = m_motionControllerRight->GetComponentTransform();

    FSplineParams params;
    params.splineUpDir = { 0, 0, 1 };

    params.pos0 = transformL.GetLocation();
    params.pos1 = transformR.GetLocation();

    params.q0 = transformL.GetRotation() * FQuat(FVector(0, 1, 0), HALF_PI) * FQuat(FVector(0, 0, 1), HALF_PI);
    params.q1 = transformR.GetRotation() * FQuat(FVector(0, -1, 0), HALF_PI) * FQuat(FVector(0, 0, -1), HALF_PI); // rotate right Quaternion in opposite direction

    params.dir0 = params.q0.GetAxisZ() * tangentLengthL;
    params.dir1 = params.q1.GetAxisZ() * tangentLengthR;

    params.roll0 = rollL;
    params.roll1 = rollR;

#define CASE_TO_FUNC(value)         \
    case EInterpolationType::value: \
        DrawSpline(&value, params); \
        break;

    switch (m_interpolationType) {
        CASE_TO_FUNC(CalcDefault)
        CASE_TO_FUNC(CalcWithShortestSpin)
        CASE_TO_FUNC(CalcWithQuatSlerp)
        CASE_TO_FUNC(CalcWithQuatCubic)
    };

    /* float angleL = atan(baseXL.Y / baseXL.X);
    float angleR = atan(baseXR.Y / baseXR.X);

    if (baseXL.X > 0)
        angleL -= PI;
    if (baseXR.X > 0)
        angleR += PI;

	auto calcPositionsByAngles = [&](float alpha) {
		auto pos = SplineEvalPos(posL, dirL, posR, dirR, alpha);
		auto SplineDir = SplineEvalDir(posL, dirL, posR, dirR, alpha);
		auto angle = FMath::Lerp(angleL, angleR, alpha);
		float sinAlpha = sin(angle), cosAlpha = cos(angle);
		auto XVec = FVector(cosAlpha, sinAlpha, 0);
		auto YVec = SplineUpDir ^ XVec;

		return TTuple<FVector, FVector, FVector>(pos, XVec, YVec);
	};*/
}
