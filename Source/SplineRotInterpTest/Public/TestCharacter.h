// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "TestCharacter.generated.h"

class UCameraComponent;
class UMotionControllerComponent;
class UTextRenderComponent;

UENUM(BlueprintType)
enum class EInterpolationType : uint8 {
    CalcDefault,
    CalcWithShortestSpin,
    CalcWithQuatSlerp,
    CalcWithQuatCubic,
    MAX
};

USTRUCT(BlueprintType)
struct FDebugDrawData {
    GENERATED_BODY()
    FVector pos, x, y;
};

USTRUCT(BlueprintType)
struct FSplineParams {
    GENERATED_BODY()
    FVector pos0, dir0, pos1, dir1;
    float roll0, roll1;
    FVector splineUpDir = { 0.f, 0.f, 1.f };

    // additional, non implementable in UE spline data
    FQuat q0, q1;
};

UCLASS()
class SPLINEROTINTERPTEST_API ATestCharacter : public ACharacter {
    GENERATED_BODY()

public:
    ATestCharacter();

protected:
#pragma region COMPONENTS
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    class USceneComponent* VROriginComponent;
    FVector InitialRelOriginLocation;
    UPROPERTY(EditDefaultsOnly)
    float cameraComebackSoftness = 1;

    /** First person camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    UCameraComponent* FirstPersonCameraComponent;

    /** Motion controller (right hand) */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    UMotionControllerComponent* m_motionControllerRight;

    /** Motion controller (left hand) */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
    UMotionControllerComponent* m_motionControllerLeft;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GripHand, meta = (AllowPrivateAccess = "true"))
    UTextRenderComponent* m_textRenderLeft;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GripHand, meta = (AllowPrivateAccess = "true"))
    UTextRenderComponent* m_textRenderRight;

#pragma endregion

#pragma region ACTION_MAPPING_AND_SETTING_PARAMETERS
    //////////// TANGENTS OF SPLINE ////////////
    //
    UPROPERTY(EditDefaultsOnly)
    float changeTangentLengthSpeed;

    void IncreaseSizeTangentL(float val);
    UPROPERTY(EditDefaultsOnly)
    float tangentLengthL;

    void IncreaseSizeTangentR(float val);
    UPROPERTY(EditDefaultsOnly)
    float tangentLengthR;

    //////////// ROLL OF SPLINE ////////////
    //
    UPROPERTY(EditDefaultsOnly)
    float rollMaxSpeed;
	float rollL, rollR;

    void IncreaseRollL(float val);
    void IncreaseRollR(float val);

    void SwitchInterpolationType();
    EInterpolationType m_interpolationType;

#pragma endregion

    //////////// DEBUG DRAW ////////////
    //
    UPROPERTY(EditDefaultsOnly)
    float debugLineLenght = 10.f;

    UPROPERTY(EditDefaultsOnly)
    uint32 m_splineResolution = 100;

	UPROPERTY(EditDefaultsOnly)
	uint32 m_divsCount = 20;

    void DrawSpline(TFunction<FDebugDrawData(const FSplineParams&, float)>, const FSplineParams& params);

    //////////// DEFAULT FUNCTIONS ////////////
    //
    void BeginPlay() override;

public:
    virtual void Tick(float DeltaTime) override;

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
